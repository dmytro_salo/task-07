package com.epam.rd.java.basic.task7.db;

public class Queries {
    //connection
    public static final String CONNECTION_URL =
            "jdbc:mysql://localhost/testdb?user=root&password=plasma97";

    //users
    public static final String SQL_FIND_ALL_USERS =
            "select * from users";

    public static final String SQL_FIND_USER_BY_LOGIN =
            "select * from users where login=?";

    public static final String SQL_CREATE_USER =
            "insert into users values (default, ?)";
//			"insert into users (login) values (?)";

    public static final String SQL_DELETE_USER =
            "delete from users where id=?";

    //teams
    public static final String SQL_CREATE_TEAM =
            "INSERT INTO teams (name) VALUES (?)";

    public static final String SQL_UPDATE_TEAM =
            "UPDATE teams SET name = ? WHERE id = ?";

    public static final String SQL_DELETE_TEAM =
            "DELETE FROM teams WHERE id = ?";

    public static final String SQL_GET_ALL_TEAMS =
            "SELECT * FROM teams";

    public static final String SQL_GET_TEAM_BY_NAME =
            "SELECT * FROM teams WHERE name = ?";

    //users_teams
    public static final String SQL_GET_USER_TEAMS =
            "SELECT t.id, t.name FROM teams t\n" +
            "JOIN users_teams ut ON t.id = ut.team_id\n" +
            "JOIN users u ON ut.user_id = u.id\n" +
            "WHERE u.id = ?";

    public static final String SQL_CREATE_USER_TEAM_PAIR =
            "INSERT INTO users_teams VALUES(?, ?)";

}
