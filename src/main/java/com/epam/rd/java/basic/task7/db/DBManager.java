package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.Queries.*;

public class DBManager {
	private static DBManager instance;
	private static final Properties APP_PROPERTIES = getAppProperties();

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		Connection con = null;
		Statement stmt;
		ResultSet rs;

		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);

			while (rs.next()) {
				users.add(extractUser(rs));
			}
		} catch (SQLException e) {
			throw new DBException("Cannot find all users", e);
		} finally {
			close(con);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(
					SQL_CREATE_USER,
					Statement.RETURN_GENERATED_KEYS);

			pstmt.setString(1, user.getLogin());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					user.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException("cannot insert user: " + user, e);
		} finally {
			close(con);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement pstmt;

		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(
					Connection.TRANSACTION_READ_COMMITTED);

			pstmt = connection.prepareStatement(
					SQL_DELETE_USER);

			for (User user : users) {
				pstmt.setString(1, String.valueOf(user.getId()));
				pstmt.executeUpdate();
			}

			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(connection);
			throw new DBException("Cannot insert users: " + Arrays.toString(users), e);
		} finally {
			close(connection);
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				user = extractUser(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		Connection connection = null;
		PreparedStatement preparedStatement;
		ResultSet resultSet;

		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(SQL_GET_TEAM_BY_NAME);
			preparedStatement.setString(1, name);

			resultSet = preparedStatement.executeQuery();

			if(resultSet.next()) {
				team = extractTeam(resultSet);
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot get team" + name, ex);
		} finally {
			close(connection);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList;
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_GET_ALL_TEAMS);
			rs = pstmt.executeQuery();

			teamList = new ArrayList<>();
			while (rs.next()) {
				teamList.add(extractTeam(rs));
			}
		} catch (SQLException ex) {
			throw new DBException("Cannot find all teams", ex);
		} finally {
			close(con);
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_CREATE_TEAM, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, team.getName());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					team.setId(rs.getInt(1));
				}
			}
		} catch (SQLException ex) {
			throw new DBException("cannot add team to database" + team, ex);
		} finally {
			close(con);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement pstmt;

		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(
					Connection.TRANSACTION_READ_COMMITTED);

			pstmt = connection.prepareStatement(
					SQL_CREATE_USER_TEAM_PAIR);

			for (Team t : teams) {
				int k = 1;
				pstmt.setString(k++, String.valueOf(user.getId()));
				pstmt.setString(k, String.valueOf(t.getId()));
				pstmt.executeUpdate();
			}

			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(connection);
			throw new DBException("Cannot create user-team pairs: " + user + Arrays.toString(teams), e);
		} finally {
			close(connection);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_GET_USER_TEAMS);
			pstmt.setString(1, String.valueOf(user.getId()));

			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException ex) {
			throw new DBException("cannot get user '" + user + "' teams from database", ex);
		} finally {
			close(con);
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			pstmt.setString(1, String.valueOf(team.getId()));
			pstmt.executeUpdate();
		} catch (SQLException ex) {
			throw new DBException("Cannot delete team from database" + team, ex);
		} finally {
			close(con);
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);
			pstmt.setString(1, team.getName());
			pstmt.setString(2, String.valueOf(team.getId()));
			pstmt.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("cannot update team" + team, e);
		} finally {
			close(con);
		}

		return true;
	}

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(APP_PROPERTIES.getProperty("connection.url"));
	}

	private static Properties getAppProperties() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(String.valueOf(Path.of("app.properties"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}

	private User extractUser(ResultSet rs) throws DBException {
		User user = new User();
		try {
			user.setId(rs.getInt("id"));
			user.setLogin(rs.getString("login"));
		} catch (SQLException e) {
			throw new DBException("Cannot extract user from ResultSet" + rs, e);
		}
		return user;
	}

	private Team extractTeam(ResultSet rs) throws DBException {
		Team team = new Team();
		try {
			team.setId(rs.getInt("id"));
			team.setName(rs.getString("name"));
		} catch (SQLException e) {
			throw new DBException("Cannot extract team from ResultSet" + rs, e);
		}
		return team;
	}

	private void close(Connection con) throws DBException {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DBException("Cannot close connection", e);
			}
		}
	}

	private void rollback(Connection con) throws DBException {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				throw new DBException("Cannot rollback query", e);
			}
		}
	}

}
